
package FuncionesFecha;

import Excepciones.FechaInvalidaException;
import com.toedter.calendar.JDateChooser;
import java.text.SimpleDateFormat;
import java.time.LocalDate;


public class FuncionFecha {
    
    SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
     
        public LocalDate getFecha(JDateChooser jdc) throws FechaInvalidaException{
           String fecha = null;
           LocalDate resultado = null;
            if(jdc.getDate()!= null){
                fecha= formato.format(jdc.getDate());
                resultado = LocalDate.parse(fecha);
            }
            if(resultado == null){
                throw new FechaInvalidaException();   
            }
            return resultado; 
       }
}
