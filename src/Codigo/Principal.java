
package Codigo;

import Excepciones.ProductoExistenteException;
import Excepciones.ProductoInexistenteException;
import InterfazGrafica.MenuPrincipal;
import java.math.BigDecimal;
import java.time.LocalDate;


public class Principal {
    
   public static void main(String[] args)throws ProductoInexistenteException, ProductoExistenteException{
       
       LocalNegocio local = new LocalNegocio();
      
         BigDecimal precioPure= new BigDecimal("76.50");
         BigDecimal precioArroz= new BigDecimal("107.67");
         BigDecimal precioDesodorante= new BigDecimal("170.00");
         BigDecimal precioShampoo= new BigDecimal("200.00");
         BigDecimal precioPapelHigienico = new BigDecimal("56.00");
       
         LocalDate fechaPure = LocalDate.of(2020,12,25);
         LocalDate fechaArroz = LocalDate.of(2023,5,23);
         LocalDate fechaDesodorante = LocalDate.of(2023,10,8);
         LocalDate fechaShampoo = LocalDate.of(2020,6,12);
         LocalDate fechaPapelHigienico = LocalDate.of(2021,5,7);
         
         
       Producto pure = new Producto("PURE DE TOMATE",precioPure,fechaPure,1258);
       Producto arroz = new Producto ("ARROZ",precioArroz,fechaArroz,5986);
       Producto desodorante = new Producto ("DESODORANTE",precioDesodorante,fechaDesodorante,59459);
       Producto shapoo = new Producto ("SHAMPOO",precioShampoo,fechaShampoo,54368);
       Producto papelHigienico = new Producto ("PAPEL HIGIENICO",precioPapelHigienico,fechaPapelHigienico,684936);
       
       local.AgregarProducto(pure);
       local.AgregarProducto(arroz);
       local.AgregarProducto(desodorante);
       local.AgregarProducto(shapoo);
       local.AgregarProducto(papelHigienico);
   
       MenuPrincipal menuPrincipal = new MenuPrincipal(local);
       menuPrincipal.setVisible(true);
   }
}
