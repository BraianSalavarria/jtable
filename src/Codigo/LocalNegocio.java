
package Codigo;

import Excepciones.ProductoExistenteException;
import Excepciones.ProductoInexistenteException;
import java.util.ArrayList;

public class LocalNegocio {

private ArrayList<Producto> productos;

public LocalNegocio(){
    this.productos = new ArrayList<Producto>();
}

    public ArrayList<Producto> getProductos() {
        return productos;
    }

   public void AgregarProducto(Producto producto) throws ProductoExistenteException{
      for(Producto var : productos){
          if(var.getCodigo().equals(producto.getCodigo())){
              throw new ProductoExistenteException();
          }
      }
       productos.add(producto);
   }

  public void EliminarProducto(Integer codigo) throws ProductoInexistenteException{
      Producto resultado = buscarProducto(codigo);
      productos.remove(resultado);
  }  

  public Producto buscarProducto(Integer codigo) throws ProductoInexistenteException{
     Producto resultado = null;
        for (Producto var : productos){
            if (var.getCodigo().equals(codigo)){
                 resultado = var;
            }
        }
         if(resultado == null){
             throw new ProductoInexistenteException("Producto Inexistente: "+ codigo);
        }
         return resultado;
  }
    
}
