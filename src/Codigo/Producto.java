
package Codigo;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Producto {

private BigDecimal precio;
private String nombre;
private LocalDate fecha;
private Integer codigo;

public Producto(String nombre, BigDecimal precio, LocalDate fecha, Integer codigo){
    
    this.nombre = nombre;
    this.precio = precio;
    this.fecha = fecha;
    this.codigo = codigo;
}

    public BigDecimal getPrecio() {
        return precio;
    }

    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }



    
}
