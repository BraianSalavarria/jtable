
package Excepciones;

/**
 *
 * @author Braian
 */
public class FechaInvalidaException extends Exception {
    
    public FechaInvalidaException(){
        super("Fecha Invalida");
    }
    
}
