
package Excepciones;

public class ProductoInexistenteException extends Exception{
    
    public ProductoInexistenteException(String mensaje){
        super(mensaje);
    }
}
