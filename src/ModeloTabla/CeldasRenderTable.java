
package ModeloTabla;

import java.awt.Color;
import java.awt.Component;
import java.time.LocalDate;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author Braian
 */
public class CeldasRenderTable extends DefaultTableCellRenderer {

    private  int FECHA = 2;
    private LocalDate condicionFecha;
    private LocalDate fechaAComparar;
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        
      condicionFecha = (LocalDate) table.getValueAt(row,FECHA);
      fechaAComparar = LocalDate.now();
      
      if(condicionFecha.isBefore(fechaAComparar)){
          this.setBackground(Color.red);
      }else{
          this.setBackground(Color.GREEN);
      }
        
        
        return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column); //To change body of generated methods, choose Tools | Templates.
    }
    






}
