
package ModeloTabla;

import Codigo.LocalNegocio;
import javax.swing.table.DefaultTableModel;


public class ModelTable extends DefaultTableModel {
 
  
private LocalNegocio localnegocio;
static Integer NOMBRE =0;
static Integer PRECIO =1;
static Integer FECHA =2;
static Integer CODIGO =3;
private String columnas[]={"NOMBRE","PRECIO","FECHA","CODIGO"};
 
public ModelTable(){
    super();
    this.setColumnIdentifiers(columnas);
}

 public void cargarDato(LocalNegocio negocio){
   
    Object fila [] = new Object[4];
    
    for(int i =0;i< negocio.getProductos().size();i++) {
        
    fila[NOMBRE] = negocio.getProductos().get(i).getNombre();
    fila[PRECIO] = negocio.getProductos().get(i).getPrecio();
    fila[FECHA] = negocio.getProductos().get(i).getFecha();
    fila[CODIGO] = negocio.getProductos().get(i).getCodigo();
    
   }
   this.addRow(fila);
}


public void cargarDatos(LocalNegocio negocio){
     Object fila [] = new Object[4];
    
    for(int i =0;i< negocio.getProductos().size();i++) {
        
    fila[NOMBRE] = negocio.getProductos().get(i).getNombre();
    fila[PRECIO] = negocio.getProductos().get(i).getPrecio();
    fila[FECHA] = negocio.getProductos().get(i).getFecha();
    fila[CODIGO] = negocio.getProductos().get(i).getCodigo();
     this.addRow(fila);
   }
}

















}
